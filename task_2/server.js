const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

app.get('/code/:code', (req, res) => {
    res.send(Vigenere.Cipher('password').crypt(req.params.code));
});

app.get('/decode/:decode', (req, res) => {
    res.send(Vigenere.Decipher('password').crypt(req.params.decode));
});

app.listen(port);